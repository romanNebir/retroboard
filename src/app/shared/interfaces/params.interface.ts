import { IComment } from '../models/comment.model';
export interface CommentParams {
    comment: IComment;
    cardId: string;
}

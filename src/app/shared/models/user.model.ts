import { IBoard } from "./board.model";

export interface IUser {
    name: string;
    boards: IBoard[];
}
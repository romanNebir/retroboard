import { IColumn } from 'src/app/shared/models/column.model';
export interface IBoard {
  id: any;
  userId: string;
  name: string;
  columns: IColumn[];
}

export class Board {
  id: any = '1';
  columns: IColumn[] = [
    {
      name: 'Went Well',
      id: '1',
      cards: [],
      color: 'green',
    },
    {
      name: 'To Improve',
      id: '2',
      cards: [],
      color: 'red',
    },
    {
      name: 'Action Items',
      id: '3',
      cards: [],
      color: 'purple',
    },
  ];
  constructor(public name: string, public userId: string) {}
}

export interface IComment {
  id: string;
  text: string;
  date: Date;
}

export class Comment implements IComment {
  id: string = Date.now().toString();
  date: Date = new Date();
  constructor(public text: string) {}
}

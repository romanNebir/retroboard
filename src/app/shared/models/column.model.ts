import { ICard } from './card.model';

export interface IColumn {
  name: string;
  id: string;
  cards: ICard[];
  color: string;
}

export class Column implements IColumn {
  public id: string = Date.now().toString();
  public cards: ICard[] = [];
  public color = 'purple';
  constructor(public name: string) {}
}

import { IComment } from './comment.model';
export interface ICard {
  id: string;
  text: string;
  comments: IComment[];
  likes: string[];
}

export class Card implements ICard {
  public id: string = Date.now().toString();
  public comments: IComment[] = [];
  public likes: string[] = [];
  constructor(public text: string) {}
}

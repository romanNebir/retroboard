import { Injectable } from '@angular/core';
import {
  Action,
  AngularFirestore,
  DocumentSnapshot,
  Query,
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { IBoard } from '../models/board.model';
import { IColumn } from '../models/column.model';

@Injectable({
  providedIn: 'root',
})
export class FacadeService {
  constructor(private firestore: AngularFirestore) {}

  public getUsersBoards(userId: string): Query<unknown> {
    return this.firestore
      .collection('boards')
      .ref.where('userId', '==', userId);
  }

  public addNewBoard(board: IBoard): any {
    return this.firestore.collection('boards').add(board);
  }

  public getBoard(id: string): Observable<Action<DocumentSnapshot<unknown>>> {
    return this.firestore.collection('boards').doc(id).snapshotChanges();
  }

  public updateBoard(board: IBoard): Promise<void> {
    return this.firestore.collection('boards').doc(board.id.toString()).update({...board});
  }
}

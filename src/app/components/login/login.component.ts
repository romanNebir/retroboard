import { Component } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  public email = '';
  public password = '';
  public invalid = false;

  constructor(private authService: AuthService, private router: Router) {}

  public login(): void {
    if (this.email && this.password) {
      this.authService
        .login(this.email, this.password)
        .then((userCredential) => {
          localStorage.setItem('userId', userCredential.user?.uid);
          this.router.navigateByUrl('boards');
        })
        .catch(() => {
          this.invalid = true;
        });
    }
  }
}

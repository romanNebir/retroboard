import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IColumn } from '../../shared/models/column.model';
import { ICard } from '../../shared/models/card.model';
import { IComment } from '../../shared/models/comment.model';
import { CommentParams } from '../../shared/interfaces/params.interface';

@Component({
  selector: 'app-column',
  templateUrl: './column.component.html',
  styleUrls: ['./column.component.scss'],
})
export class ColumnComponent {
  @Input() public column: IColumn;
  @Output() public addCardEvent: EventEmitter<string> = new EventEmitter();
  @Output() public deleteColumnEvent: EventEmitter<string> = new EventEmitter();
  @Output() public changeColorEvent: EventEmitter<string> = new EventEmitter();
  @Output() public deleteCardEvent: EventEmitter<string> = new EventEmitter();
  @Output() public editCardEvent: EventEmitter<ICard> = new EventEmitter();
  @Output() public editNameEvent: EventEmitter<string> = new EventEmitter();
  @Output() public addCommentEvent: EventEmitter<{text: string, cardId: string}> = new EventEmitter();
  @Output() public editCommentEvent: EventEmitter<CommentParams> = new EventEmitter();
  @Output() public deleteCommentEvent: EventEmitter<CommentParams> = new EventEmitter();
  @Output() public likeEvent: EventEmitter<string> = new EventEmitter();

  public createCard = false;
  public columnColors: string[] = [
    'green',
    'red',
    'purple',
    'blue',
    'dodgerblue',
    'orange',
  ];
  public editNameMode = false;
  public columnName: string;

  public deleteColumn(column: IColumn): void {
    this.deleteColumnEvent.emit(column.id);
  }

  public editNameModeOn(): void {
    this.columnName = this.column.name;
    this.editNameMode = true;
  }

  public editName(): void {
    if (this.columnName) {
      this.editNameEvent.emit(this.columnName);
      this.editNameMode = false;
    }
  }

  public cancelAdding(): void {
    this.createCard = false;
  }

  public addCard(text: string): void {
    this.addCardEvent.emit(text);
    this.createCard = false;
  }

  public changeColor(color: string): void {
    this.changeColorEvent.emit(color);
  }

  public deleteCard(card: ICard): void {
    this.deleteCardEvent.emit(card.id);
  }

  public editCard(text: string, card: ICard): void {
    this.editCardEvent.emit({ id: card.id, text } as ICard);
  }

  public addComment(text: string, card: ICard): void {
    this.addCommentEvent.emit({text, cardId: card.id});
  }

  public editComment(comment: IComment, card: ICard): void {
    this.editCommentEvent.emit({comment, cardId: card.id});
  }

  public deleteComment(comment: IComment, card: ICard): void {
    this.deleteCommentEvent.emit({comment, cardId: card.id});
  }

  public like(card: ICard): void {
    this.likeEvent.emit(card.id);
  }
}

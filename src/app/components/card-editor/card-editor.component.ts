import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-card-editor',
  templateUrl: './card-editor.component.html',
  styleUrls: ['./card-editor.component.scss'],
})
export class CardEditorComponent{
  @Input() public color: string;
  @Input() public text: string;
  @Output() public confirmEvent: EventEmitter<string> = new EventEmitter();
  @Output() public cancelEvent: EventEmitter<void> = new EventEmitter();

  confirm(): void {
    if (this.text) {
      this.confirmEvent.emit(this.text);
    }
  }

  cancel(): void {
    this.cancelEvent.emit();
  }
}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ICard } from '../../shared/models/card.model';
import { IComment } from '../../shared/models/comment.model';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit{
  @Input() public card: ICard;
  @Input() public color: string;
  @Output() public deleteCardEvent: EventEmitter<void> = new EventEmitter();
  @Output() public editCardEvent: EventEmitter<string> = new EventEmitter();
  @Output() public addCommentEvent: EventEmitter<string> = new EventEmitter();
  @Output() public editCommentEvent: EventEmitter<IComment> = new EventEmitter();
  @Output() public deleteCommentEvent: EventEmitter<IComment> = new EventEmitter();
  @Output() public likeEvent: EventEmitter<void> = new EventEmitter();


  public editMode = false;
  public openComments = false;
  public commentText = '';
  public liked: boolean;

  ngOnInit(): void {
    this.checkIfLiked();
  }

  public editCard(text: string): void {
    this.editCardEvent.emit(text);
    this.editMode = false;
  }

  public cancelEditing(): void {
    this.editMode = false;
  }

  public deleteCard(): void {
    this.deleteCardEvent.emit();
  }

  public addComment(): void {
    if (this.commentText) {
      this.addCommentEvent.emit(this.commentText);
    }
  }

  public editComment(editedComment: IComment): void {
    this.editCommentEvent.emit(editedComment);
  }

  public deleteComment(comment: IComment): void {
    this.deleteCommentEvent.emit(comment);
  }

  public like(): void {
    this.likeEvent.emit();
  }

  public checkIfLiked(): void {
    if (this.card.likes.includes(localStorage.getItem('userId') as string)) {
      this.liked = true;
    } else {
      this.liked = false;
    }
  }
}

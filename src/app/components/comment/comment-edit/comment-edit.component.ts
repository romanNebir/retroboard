import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IComment } from '../../../shared/models/comment.model';

@Component({
  selector: 'app-comment-edit',
  templateUrl: './comment-edit.component.html',
  styleUrls: ['./comment-edit.component.scss']
})
export class CommentEditComponent implements OnInit {
  @Input() public comment: IComment;
  @Output() public cancelEditingEvent: EventEmitter<void> = new EventEmitter();
  @Output() public editCommentEvent: EventEmitter<IComment> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  public cancel(): void {
    this.cancelEditingEvent.emit();
  }

  public editComment(): void {
    if (this.comment.text) {
      this.editCommentEvent.emit(this.comment);
    }
  }

}

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IComment } from '../../../shared/models/comment.model';

@Component({
  selector: 'app-comment-view',
  templateUrl: './comment-view.component.html',
  styleUrls: ['./comment-view.component.scss']
})
export class CommentViewComponent {
  @Input() public comment: IComment;
  @Output() public switchToEditEvent: EventEmitter<void> = new EventEmitter();
  @Output() public deleteCommentEvent: EventEmitter<void> = new EventEmitter();

  public switchToEdit(): void {
    this.switchToEditEvent.emit();
  }

  public deleteComment(): void {
    this.deleteCommentEvent.emit();
  }

}

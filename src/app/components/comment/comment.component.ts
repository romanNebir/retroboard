import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IComment } from '../../shared/models/comment.model';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss'],
})
export class CommentComponent {
  @Input() public comment: IComment;
  @Output() public editCommentEvent: EventEmitter<IComment> = new EventEmitter();
  @Output() public deleteCommentEvent: EventEmitter<IComment> = new EventEmitter();

  public editMode = false;

  public editComment(newComment: IComment): void {
    this.editCommentEvent.emit(newComment);
  }

  public deleteComment(): void {
    this.deleteCommentEvent.emit(this.comment);
  }
}

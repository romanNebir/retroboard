import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  public email = '';
  public password = '';
  public invalid = false;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {}

  public register(): void {
    if (this.email && this.password) {
      this.authService
        .register(this.email, this.password)
        .then((userCredential) => {
          localStorage.setItem('userId', userCredential.user?.uid);
          this.router.navigateByUrl('boards');
        })
        .catch(() => {
          this.invalid = true;
        });
    }
  }
}

import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Column, IColumn } from 'src/app/shared/models/column.model';
import { Card, ICard } from 'src/app/shared/models/card.model';
import { IBoard } from '../../shared/models/board.model';
import { ActivatedRoute } from '@angular/router';
import { FacadeService } from '../../shared/services/facade.service';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { IComment, Comment } from '../../shared/models/comment.model';
import { CommentParams } from '../../shared/interfaces/params.interface';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
})
export class BoardComponent implements OnInit {
  public board: IBoard;
  public modalRef: BsModalRef;
  public columnName = '';
  public listConnectedTo: string[] = [];

  constructor(
    private modalService: BsModalService,
    private actRoute: ActivatedRoute,
    private facadeService: FacadeService
  ) {}

  ngOnInit(): void {
    const boardId = this.actRoute.snapshot.paramMap.get('id') as string;
    this.facadeService.getBoard(boardId).subscribe((doc) => {
      const data = doc.payload.data() as IBoard;
      const id = doc.payload.id;
      this.board = { ...data, id };
      this.board.columns.forEach((col) => this.listConnectedTo.push(col.name));
    });
  }

  openModal(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template);
  }

  closeModal(): void {
    this.columnName = '';
    this.modalRef.hide();
  }

  addColumn(): void {
    if (this.columnName) {
      this.board.columns.push({ ...new Column(this.columnName) });
      this.facadeService
        .updateBoard(this.board)
        .catch((err) => console.log(err));
      this.closeModal();
    }
  }

  deleteColumn(columnId: string): void {
    this.board.columns = this.board.columns.filter(
      (column) => column.id !== columnId
    );
    this.facadeService.updateBoard(this.board).catch((err) => console.log(err));
  }

  editName(name: string, column: IColumn): void {
    (this.board.columns.find(
      (elem) => elem.id === column.id
    ) as IColumn).name = name;
    this.facadeService.updateBoard(this.board).catch((err) => console.log(err));
  }

  addCard(text: string, column: IColumn): void {
    const newCard = new Card(text);
    this.board.columns
      .find((elem) => elem.id === column.id)
      ?.cards.push({ ...newCard });
    this.facadeService.updateBoard(this.board).catch((err) => console.log(err));
  }

  changeColor(color: string, column: IColumn): void {
    (this.board.columns.find(
      (elem) => elem.id === column.id
    ) as IColumn).color = color;
    this.facadeService.updateBoard(this.board).catch((err) => console.log(err));
  }

  deleteCard(cardId: string, column: IColumn): void {
    const columnIndex = this.board.columns.findIndex(
      (col) => col.id === column.id
    );
    this.board.columns[columnIndex].cards = this.board.columns[
      columnIndex
    ].cards.filter((card) => card.id !== cardId);
    this.facadeService.updateBoard(this.board).catch((err) => console.log(err));
  }

  editCard(params: ICard, column: IColumn): void {
    const columnIndex = this.board.columns.findIndex(
      (col) => col.id === column.id
    );
    const cardIndex = this.board.columns[columnIndex].cards.findIndex(
      (card) => card.id === params.id
    );
    this.board.columns[columnIndex].cards[cardIndex].text = params.text;
    this.facadeService.updateBoard(this.board).catch((err) => console.log(err));
  }

  drop(event: CdkDragDrop<ICard[]>, column: IColumn): void {
    if (event.container.id === event.previousContainer.id) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
    this.facadeService.updateBoard(this.board).catch((err) => console.log(err));
  }

  public addComment(params: any, column: IColumn): void {
    const newComment = { ...new Comment(params.text) };
    this.board.columns
      .find((col) => col.id === column.id)
      ?.cards.find((card) => card.id === params.cardId)
      ?.comments.push(newComment);
    this.facadeService.updateBoard(this.board).catch((err) => console.log(err));
  }

  public editComment(params: CommentParams, column: IColumn): void {
    const comment = this.board.columns
      .find((col) => col.id === column.id)
      ?.cards.find((card) => card.id === params.cardId)
      ?.comments.find((com) => com.id === params.comment.id);
    (comment as IComment).text = params.comment.text;
    this.facadeService.updateBoard(this.board).catch((err) => console.log(err));
  }

  public deleteComment(params: CommentParams, column: IColumn): void {
    const card = this.board.columns
      .find((col) => col.id === column.id)
      ?.cards.find((elem) => elem.id === params.cardId);
    (card as ICard).comments = (card as ICard).comments.filter(
      (com) => com.id !== params.comment.id
    );
    this.facadeService.updateBoard(this.board).catch((err) => console.log(err));
  }

  public like(cardId: string, column: IColumn): void {
    const card = this.board.columns
      .find((col) => col.id === column.id)
      ?.cards.find((elem) => elem.id === cardId);
    const userId = localStorage.getItem('userId') as string;
    if (card?.likes.includes(userId)) {
      card.likes = card.likes.filter(elem => elem !== userId);
    } else {
      card?.likes.push(userId);
    }
    this.facadeService.updateBoard(this.board).catch((err) => console.log(err));
  }

}

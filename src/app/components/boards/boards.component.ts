import { Component, OnInit, TemplateRef } from '@angular/core';
import { FacadeService } from '../../shared/services/facade.service';
import { Board, IBoard } from '../../shared/models/board.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.scss'],
})
export class BoardsComponent implements OnInit {
  public userId: string;
  public boards: IBoard[] = [];
  public modalRef: BsModalRef;
  public boardName = '';

  constructor(
    private facadeService: FacadeService,
    private modalService: BsModalService,
    private router: Router
  ) {}

  ngOnInit(): void {
    if (localStorage.getItem('userId')) {
      this.userId = localStorage.getItem('userId') as string;
      this.getBoards();
    } else {
      this.router.navigate(['/login']);
    }
  }

  public getBoards(): void {
    this.facadeService.getUsersBoards(this.userId).onSnapshot((collection) => {
      this.boards = [];
      collection.forEach((document) => {
        const data = document.data() as IBoard;
        const id = document.id;
        this.boards.push({ ...data, id });
      });
    });
  }

  public openModal(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template);
  }

  public closeModal(): void {
    this.modalRef.hide();
    this.boardName = '';
  }

  public addBoard(): void {
    if (this.boardName) {
      const newBoard = new Board(this.boardName, this.userId);
      delete newBoard.id;
      this.facadeService.addNewBoard(Object.assign({}, newBoard));
      this.closeModal();
    }
  }

  public goToBoard(board: IBoard): void {
    this.router.navigate([`/board/${board.id}`]);
  }
}

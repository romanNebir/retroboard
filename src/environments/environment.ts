// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBVCGRw0IONQ4diSx5SNtXilE3I1aC4ytA',
    authDomain: 'retroboard-61ffd.firebaseapp.com',
    projectId: 'retroboard-61ffd',
    storageBucket: 'retroboard-61ffd.appspot.com',
    messagingSenderId: '285350336562',
    appId: '1:285350336562:web:dc64441e61a2e51670042b',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
